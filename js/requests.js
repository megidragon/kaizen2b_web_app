const BASE_URL = 'http://157.230.228.158/api/';

function request(method, url, data={}, callback){
    headers = {
        'Accept': 'application/json',
        'Authorization': readCookie('token')
    };

    if (method.toLocaleLowerCase() == 'get'){
        $.ajax({
            type: 'GET',
            url: BASE_URL+url,
            headers: headers,
            success: callback,
            fail: function (xhr, textStatus, errorThrown) {
                alert('request failed');
            }
        });
    }else{
        $.ajax({
            type: method,
            url: BASE_URL+url,
            headers: headers,
            data: data,
            dataType: 'json',
            success: callback,
            fail: function (xhr, textStatus, errorThrown) {
                alert('request failed');
            }
        });
    }

    return;

    if (method == 'POST'){
        $.post(BASE_URL+url, data, callback, 'json');
    }else if (method == 'GET'){
        $.get(BASE_URL+url, callback, 'json');
    }else if (method == 'PATH'){
        $.post(BASE_URL+url, data, callback, 'json');
    }
}

function loginWs(cuit, email, pass, remember=false){
    $.post(BASE_URL+'login', {
        'cuit': cuit,
        'email': email,
        'password': pass
    },function (res) {
        if (res.success){
            createCookie('token', res.token, remember);
            getUserDetailsWs();
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'En 3 segundos sera redireccionado.',
                showConfirmButton: false,
                timer: 3500
            })
            setTimeout(function () {
                document.location.replace('activities.php');
            }, 3000)
        }else if(typeof res.error != 'undefined' && typeof res.error.msg != 'undefined'){
            alert('Error al iniciar sesion: '+res.error.msg);
        }
    }, 'json');
}

function logout(){
    eraseCookie('token');
}

function getUserDetailsWs(){
    if (!readCookie('token')){
        return;
    }

    request('GET', 'user', null, function(res){
        if (res.success){
            console.log(res);
            alert('Recibido')
        }else{
            if (res.error){
                alert('Error al pedir datos: '+res.error.msg);
            }else{
                console.log(res);
                alert('Error fatal del sistema');
            }
        }
    }, 'json');
}
